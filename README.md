# Kinsa Coffee Shop API

## API structure

This is a REST API for coffee shops, or cafes.

The following combinations of HTTP method and endpoint are supported:

```
GET /cafes/:id
DELETE /cafes/:id
PUT /cafes/:id
POST /cafes
POST /find_nearest
```

More comprehensive documentation can be found [here](https://documenter.getpostman.com/view/28663/RWaDVqin#3604eb6b-13ab-47ca-8296-116baaa560e7).

This application uses Node.js Express and is written in TypeScript. We use the Google Maps API to geocode addresses when finding the nearest coffee shops. The algorithm powering the `/find_nearest` endpoint is based on the [Haversine formula](https://en.wikipedia.org/wiki/Haversine_formula).

I used [Postman](https://www.getpostman.com/) to help build the app and automatically generate the linked documentation. The Postman collection is included with this repository in `./kinsa_cafe_api.postman_collection.json`.

## Development

### dependencies

- **node >=8.0.0**

  It is recommended to install node.js via [nvm](https://github.com/creationix/nvm) using their [install script](https://github.com/creationix/nvm#install-script).

  This app was tested using node versions `8.4.0` and `10.9.0`.

- **yarn v1.9** [installation instructions](https://yarnpkg.com/en/docs/install#alternatives-tab)

### installation

`$ yarn`

A `node_modules` directory will be created.

Create a `.env` file in the root directory of the project. Add your Google Cloud Platform API key to this file:

```
GOOGLE_CLOUD_API=YOUR_KEY
```

### helpful project commands

All commands begin with `yarn` e.g. `$ yarn build`

- `build`

  Runs a local server at [http://localhost:5000](http://localhost:5000)

  The project is rebuilt and reloaded automatically when files are changed.

  The port can be changed by passing an environmental variable:

  `$ PORT=7000 yarn build`

- `build:inspect`

  Runs the node inspector, which you can interact with using Chrome's developer tools,

  by opening `about:inspect` in Chrome and clicking on the _Open dedicated DevTools for Node_ link.

* `start`

  Compiles the Typescript and runs a local server with the compiled JavaScript.

* `test`

  Runs test suite one time.

* `test:watch`

  Runs test suite continually while watching for for file changes.
