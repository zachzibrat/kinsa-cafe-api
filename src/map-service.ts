import { createClient, RequestHandle } from '@google/maps'
import { CafeList, ICafe, ILocation } from './cafe-service'

const { asin, sin, cos, pow, sqrt, PI } = Math
const toRadians = degrees => (degrees * PI) / 180

const EARTH_RADIUS = 6378137

const computeDistanceBetween = (
  { latitude: fromLat, longitude: fromLng }: ICafe,
  { latitude: toLat, longitude: toLng }: ILocation,
  radius = EARTH_RADIUS,
): number => {
  const fromLatRad = toRadians(fromLat)
  const fromLngRad = toRadians(fromLng)
  const toLatRad = toRadians(toLat)
  const toLngRad = toRadians(toLng)
  const determinant =
    pow(sin((fromLatRad - toLatRad) / 2), 2) +
    cos(fromLatRad) * cos(toLatRad) * pow(sin((fromLngRad - toLngRad) / 2), 2)
  return 2 * radius * asin(sqrt(determinant))
}

export default class MapService {
  private mapClient
  constructor(apiKey = process.env.GOOGLE_CLOUD_API) {
    this.mapClient = createClient({ key: apiKey, Promise })
  }

  public geocodeAddress(address: string): Promise<RequestHandle> {
    return this.mapClient.geocode({ address }).asPromise()
  }

  public getClosestCafeForLocation(
    { lat: latitude, lng: longitude }: { lat: number; lng: number },
    list: CafeList,
  ): { distance: number; coffeeShop: ICafe } {
    let coffeeShop!: ICafe
    let minDistance: number = Infinity
    let distance: number

    for (const cafe of list.toJS()) {
      distance = computeDistanceBetween(cafe, { latitude, longitude })
      if (distance < minDistance) {
        minDistance = distance
        coffeeShop = cafe
      }
    }

    return { distance: minDistance, coffeeShop }
  }
}
