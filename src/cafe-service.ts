import { List } from 'immutable'
import { Request, Response, NextFunction } from 'express'
import createError from 'http-errors'
import { validate, object, string, number } from 'joi'

import MapService from './map-service'
import { wrapAsync } from './util/wrap-async'

const schema = object().keys({
  address: string().required(),
  latitude: number().required(),
  longitude: number().required(),
  name: string().required(),
})

export interface ILocation {
  latitude: number
  longitude: number
}
export interface ICafe extends ILocation {
  id: number
  name: string
  address: string
}

export type CafeList = List<ICafe>

export const notFoundError = id => {
  return createError(404, `cannot find coffee shop with id: ${id}`)
}

const makeRecord = (id, payload): ICafe => ({ ...payload, id })

export default class CafeService {
  private cafeStore: CafeList
  private mapService: MapService
  private idCounter: number

  constructor(data) {
    let idCounter = 0

    const records: ICafe[] = data
      .toString()
      .split('\n')
      .map(row => {
        const [id, name, address, latitude, longitude] = row.split(', ')
        idCounter = Math.max(+id, idCounter)
        return { id: +id, name, address, latitude: +latitude, longitude: +longitude }
      })

    // add 1 and set to initial value
    this.idCounter = ++idCounter
    this.cafeStore = List(records)
    this.mapService = new MapService()

    this.read = this.read.bind(this)
    this.create = this.create.bind(this)
    this.update = this.update.bind(this)
    this.destroy = this.destroy.bind(this)
    this.findNearest = this.findNearest.bind(this)
  }

  public read(req: Request, res: Response, next: NextFunction) {
    const {
      params: { id: requestedID },
    } = req
    const result = this.cafeStore.find(({ id }: any) => id === +requestedID)
    if (!result) {
      next(notFoundError(requestedID))
    } else {
      res.json(result)
    }
  }

  public create(req: Request, res: Response, next: NextFunction) {
    const { body } = req

    const recordIdx = this.cafeStore.findIndex(({ name, address }: any) => {
      // rudimentary pattern to check if record already exists
      // can refine with things like fuzzy matching against name and address
      return name === body.name && address === body.address
    })

    if (recordIdx > -1) {
      return next(createError(422, 'record already exists'))
    }

    const { error, value: validatedPayload } = validate(body, schema)

    if (error) {
      return next(createError(422, { error }))
    }
    // generate ID
    const id = this.idCounter++
    const record = makeRecord(id, validatedPayload)

    this.cafeStore = this.cafeStore.push(record)
    return res.status(201).json({ id })
  }

  public update(req: Request, res: Response, next: NextFunction) {
    const {
      params: { id: requestedID },
      body,
    } = req
    const idx = this.cafeStore.findIndex(({ id }: any) => id === +requestedID)
    if (idx < 0) {
      return next(notFoundError(requestedID))
    }

    const { error, value: validatedPayload } = validate(body, schema)

    if (error) {
      return next(createError(422, { error }))
    }

    const record = makeRecord(+requestedID, validatedPayload)
    this.cafeStore = this.cafeStore.update(idx, () => record)
    return res.sendStatus(200)
  }

  public destroy(req: Request, res: Response, next: NextFunction) {
    const {
      params: { id: requestedID },
    } = req
    const idx = this.cafeStore.findIndex(({ id }: any) => id === +requestedID)
    if (idx > -1) {
      this.cafeStore = this.cafeStore.delete(idx)
      return res.sendStatus(204)
    } else {
      return next(notFoundError(requestedID))
    }
  }

  public async findNearest(req: Request, res: Response, next: NextFunction) {
    const {
      body: { address },
    } = req
    if (!address) {
      return next(createError(422, 'please include an address field'))
    }

    const [error, response] = await wrapAsync(this.mapService.geocodeAddress(address))

    if (error) {
      return next(createError(422, error.json.error_message))
    }

    const { json } = response

    if (!json.results) {
      return next(createError(404, 'unable to geocode address'))
    }

    const [
      {
        geometry: { location },
      },
    ] = json.results

    const data = this.mapService.getClosestCafeForLocation(location, this.cafeStore)
    return res.json(data)
  }
}
