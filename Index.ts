import dotenv from 'dotenv'
dotenv.config()

import App from './src/app'

const app = new App(process.env.PORT || '5000')
app.start()
