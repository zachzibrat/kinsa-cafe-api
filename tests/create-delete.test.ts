import chai from 'chai'
import { describe, it } from 'mocha'
import { server } from './app.test'

const { expect, request } = chai

describe('POST /cafes & DELETE /cafes/:id', async () => {
  it('incomplete payloads for `create` fail with a 422 error and return list of missing fields', async () => {
    const { status, body } = await request(server)
      .post(`/cafes`)
      .send({
        address: '234 chestnut',
        name: 'I do not have all my parts',
      })
    expect(status).to.equal(422)
    expect(body.error).to.exist
  })

  let createdID

  it('can create a new coffee shop entry', async () => {
    const { status, body } = await request(server)
      .post(`/cafes`)
      .send({
        name: 'amazing brand new the best',
        address: 'here',
        latitude: 20,
        longitude: 30,
      })

    expect(status).to.equal(201)
    expect(body.id).to.exist
    createdID = body.id
  })

  it('can delete entries', async () => {
    const deleteResponse = await request(server).del(`/cafes/${createdID}`)
    expect(deleteResponse.status).to.equal(204)

    const deletedRecordResponse = await request(server).del(`/cafes/${createdID}`)
    expect(deletedRecordResponse.status).to.equal(404)
  })
})
