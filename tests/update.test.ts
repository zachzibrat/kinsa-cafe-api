import chai from 'chai'
import { describe, it } from 'mocha'
import { server } from './app.test'

const { expect, request } = chai

describe('PUT /cafes/:id', () => {
  it('can update an existing record with new data', async () => {
    const updatingID = 4
    const newRecord = { name: 'new name', address: 'here', latitude: 100, longitude: 90 }

    const updateErrorResponse = await request(server)
      .put(`/cafes/${updatingID}`)
      .send({ name: 'new name' })

    expect(updateErrorResponse.status).to.equal(422)
    expect(updateErrorResponse.body.error).to.exist

    const updateSuccessResponse = await request(server)
      .put(`/cafes/${updatingID}`)
      .send(newRecord)

    expect(updateSuccessResponse.status).to.equal(200)

    const { body } = await request(server).get(`/cafes/${updatingID}`)
    expect(body).to.deep.equal({ ...newRecord, id: updatingID })
  })
})
