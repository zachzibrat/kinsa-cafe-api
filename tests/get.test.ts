import chai from 'chai'
import { describe, it } from 'mocha'
import { server } from './app.test'

const { expect, request } = chai

describe('GET /cafes/:id', () => {
  it('reads a cafe using an ID', async () => {
    const ID = 1

    const response = await request(server).get(`/cafes/${ID}`)

    expect(response).to.be.json
    expect(response.status).to.equal(200)
    expect(response.body.id).to.equal(ID)
  })

  it('returns 404 if coffee shop ID cannot be found', async () => {
    const ID = 344

    const { status } = await request(server).get(`/cafes/${ID}`)
    expect(status).to.equal(404)
  })
})
